﻿
# Pepper and Carrot MINI fonts

Open fonts used in the derivative webcomic [Pepper and Carrot MINI](https://www.peppercarrot.com/?static11/community-webcomics&page=Pepper-and-Carrot-Mini_by_Nartance/) are the same used in the main webcomic. So, I advise you to visit [this link](https://framagit.org/peppercarrot/webcomics/tree/master/fonts) if you need a specific font for a specific language. You can find fonts and their licenses. 

## Nice, but what fonts do you use among all of these?

All fonts used in Pepper and Carrot MINI are listed in the file `fonts_used.README`, with the link to the main folder where you can fond it with licenses and copyrights.

## And if I want to add MY font?

You can add fonts on the main repo, following the instructions of the README file. Once it is done, you can update the file `fonts_used.README` to add it to the list. If you use the same fonts as another language, just ad your language at the end of the specific fonts.